/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Natthakritta
 */
public class TestUerService {

    public static void main(String[] args) {

        //check add
        UserService.addUser("user2", "password");

        //check getUsers
        System.out.println(UserService.getUsers());
        UserService.addUser(new User("user3", "password"));
        System.out.println(UserService.getUsers());
        UserService.addUser(new User("user2", "password"));
        System.out.println(UserService.getUsers());

        User user = UserService.getUser(4);
        System.out.println(user);

        //check setUserName
        user.setUserName("user4");
        System.out.println(user);

        //check updateUser
        UserService.updateUser(3, user);

        //check getUser(index)
        System.out.println(UserService.getUser(3));
        System.out.println(UserService.getUsers());

        //check delUser
        UserService.delUser(3);
        System.out.println(UserService.getUsers());
    }
}
